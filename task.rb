def max_prime_factor(num)
  return num if num == 1 || num < 1
  max_num = (Math.sqrt(num) + 1).to_i
  f = (2..max_num).find { |x| num % x == 0 } || num
  [f, max_prime_factor(num / f)].max
end

number = 1876191413

puts "The largest prime factor of #{number} is #{max_prime_factor(number)}"
